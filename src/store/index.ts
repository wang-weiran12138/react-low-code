import { configureStore } from "@reduxjs/toolkit"
import userSlice from "./modules/users"
const store = configureStore({
    reducer:{
        user:userSlice.reducer
    }
})
export type RootState = ReturnType<typeof store.getState>
export type RootDispath = typeof store.dispatch
export default store