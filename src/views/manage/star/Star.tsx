import { FunctionComponent } from 'react';
import QuestionCard from '../../../components/questionCard/questionCard';
import styles from './Star.module.scss';
import { Empty, Spin } from 'antd';
import ListSearch from '../../../components/listSearch/ListSearch';
import useLoadQuestionList from '../../../hooks/requestHooks/useLoadQuestionList';
import ListPagination from '../../../components/pagination/Pagination';
interface StarProps {
  [key: string]: any;
}
const Star: FunctionComponent<StarProps> = () => {
  // const [questionList, setQuestionList] = useState([
  //   {
  //     id: 'q1',
  //     title: '问卷1',
  //     isStar: true,
  //     isPublished: false,
  //     answerCount: 5,
  //     createAt: '3月10日 13:23',
  //   },
  //   {
  //     id: 'q2',
  //     title: '问卷2',
  //     isStar: true,
  //     isPublished: true,
  //     answerCount: 3,
  //     createAt: '3月10日 13:23',
  //   },
  //   {
  //     id: 'q3',
  //     title: '问卷3',
  //     isStar: true,
  //     isPublished: false,
  //     answerCount: 1,
  //     createAt: '3月10日 13:23',
  //   },
  //   {
  //     id: 'q4',
  //     title: '问卷4',
  //     isPublished: true,
  //     isStar: true,
  //     answerCount: 2,
  //     createAt: '3月10日 13:23',
  //   },
  // ]);
  const { data = {}, loading } = useLoadQuestionList({
    isStar: true,
    isDeleted: false,
  });
  const { data: list, total } = data;

  return (
    <>
      <div className={styles.container}>
        <div className={styles.header}>
          <div className={styles.left}>
            <h3>星标问卷</h3>
          </div>
          <div className={styles.right}>
            <ListSearch />
          </div>
        </div>
        <div className={styles.main}>
          {loading && (
            <div style={{ textAlign: 'center' }}>
              <Spin />
            </div>
          )}
          {list?.length > 0 &&
            list.map((item: any) => (
              <QuestionCard key={item.id} {...item}></QuestionCard>
            ))}
          {!loading && list?.length === 0 && <Empty description="暂无数据" />}
        </div>
        <div>
          {!loading && list?.length > 0 && <ListPagination total={total} />}
        </div>
      </div>
    </>
  );
};

export default Star;
