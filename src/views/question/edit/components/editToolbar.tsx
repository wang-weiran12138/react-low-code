import {
  CopyOutlined,
  DeleteOutlined,
  DownOutlined,
  EyeInvisibleOutlined,
  LockOutlined,
  SnippetsOutlined,
  UnlockOutlined,
  UpOutlined,
} from '@ant-design/icons';
import { Button, Space, Tooltip } from 'antd';
import { FunctionComponent } from 'react';
import { useGetComponentInfo } from '../../../../hooks/useGetComponentInfo';
const EditToolbar: FunctionComponent = () => {
  const {
    components,
    removeSelectedComponent,
    selectedId,
    hideOrShowSelectedComponent,
    LockorUnlockSelectedComponent,
    PasteSelectedComponent,
    copySelectedComponent,
    selectedComponent,
    copiedComponent,
    moveComponent,
  } = useGetComponentInfo();

  const hanlderDelete = () => {
    removeSelectedComponent(selectedId);
  };
  const hanlderHide = () => {
    hideOrShowSelectedComponent(selectedId);
  };
  const hanlderLock = () => {
    LockorUnlockSelectedComponent(selectedId);
  };
  const hanlderCopy = () => {
    copySelectedComponent(selectedId);
  };
  const hanlderPaste = () => {
    PasteSelectedComponent();
  };
  const hanlderUpOrDown = (type: 'up' | 'down') => {
    if (type === 'up') {
      if (selectedId === components[0].id) return;
      const oldIndex = components.findIndex((item) => item.id === selectedId);
      const newIndex = oldIndex - 1;
      moveComponent(oldIndex, newIndex);
    } else {
      if (selectedId === components[components.length - 1].id) return;
      const oldIndex = components.findIndex((item) => item.id === selectedId);
      const newIndex = oldIndex + 1;
      moveComponent(oldIndex, newIndex);
    }
  };
  return (
    <Space>
      <Tooltip title="删除">
        <Button disabled={!selectedId} shape="circle" icon={<DeleteOutlined />} onClick={hanlderDelete} />
      </Tooltip>
      <Tooltip title="隐藏">
        <Button disabled={!selectedId} shape="circle" icon={<EyeInvisibleOutlined />} onClick={hanlderHide} />
      </Tooltip>
      <Tooltip title="锁定">
        <Button
          disabled={!selectedId}
          shape="circle"
          type={selectedComponent?.isLocked ? 'primary' : 'default'}
          icon={selectedComponent?.isLocked ? <LockOutlined /> : <UnlockOutlined />}
          onClick={hanlderLock}
        />
      </Tooltip>
      <Tooltip title="复制">
        <Button shape="circle" disabled={!selectedId} icon={<CopyOutlined />} onClick={hanlderCopy} />
      </Tooltip>
      <Tooltip title="粘贴">
        <Button
          disabled={copiedComponent ? false : true}
          shape="circle"
          icon={<SnippetsOutlined />}
          onClick={hanlderPaste}
        />
      </Tooltip>
      <Tooltip title="上移">
        <Button
          disabled={!selectedId || selectedId === components[0].id}
          shape="circle"
          icon={<UpOutlined />}
          onClick={() => hanlderUpOrDown('up')}
        />
      </Tooltip>
      <Tooltip title="下移">
        <Button
          disabled={!selectedId || selectedId === components[components.length - 1].id}
          shape="circle"
          icon={<DownOutlined />}
          onClick={() => hanlderUpOrDown('down')}
        />
      </Tooltip>
    </Space>
  );
};

export default EditToolbar;
