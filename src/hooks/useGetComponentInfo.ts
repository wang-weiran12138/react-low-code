import useQuestionStore from '../zustandStore/questionStore';
export const useGetComponentInfo = () => {
  const selectedId = useQuestionStore((state) => state.selectedId);
  const components = useQuestionStore((state) => state.components);
  const changeSelectedId = useQuestionStore((state) => state.changeSelectedId);
  const copiedComponent = useQuestionStore((state) => state.copiedComponent);
  const removeSelectedComponent = useQuestionStore((state) => state.removeSelectedComponent);
  const hideOrShowSelectedComponent = useQuestionStore((state) => state.hideOrShowSelectedComponent);
  const LockorUnlockSelectedComponent = useQuestionStore((state) => state.LockorUnlockSelectedComponent);
  const copySelectedComponent = useQuestionStore((state) => state.copySelectedComponent);
  const PasteSelectedComponent = useQuestionStore((state) => state.PasteSelectedComponent);
  const selectPrevComponent = useQuestionStore((state) => state.selectPrevComponent);
  const selectNextComponent = useQuestionStore((state) => state.selectNextComponent);
  const selectedComponent = components.find((c) => c.id === selectedId);
  const setComponentInfo = useQuestionStore((state) => state.setComponentInfo);
  const hideOrShowSelectedComponentOnLayers = useQuestionStore((state) => state.hideOrShowSelectedComponentOnLayers);
  const moveComponent = useQuestionStore((state) => state.moveComponent);

  return {
    changeSelectedId,
    components,
    copiedComponent,
    removeSelectedComponent,
    selectedId,
    hideOrShowSelectedComponent,
    LockorUnlockSelectedComponent,
    selectedComponent,
    copySelectedComponent,
    PasteSelectedComponent,
    selectNextComponent,
    selectPrevComponent,
    setComponentInfo,
    hideOrShowSelectedComponentOnLayers,
    moveComponent,
  };
};
