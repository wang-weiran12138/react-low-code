// import { useDispatch, useSelector } from "react-redux";
// import { RootState, RootDispath } from "./store";
// import { changeName, setStateList, userQuery } from "./store/modules/users";
import 'antd/dist/reset.css';
import Home from './views/home/Home';
import List from './views/manage/list/list';
import './index.scss';
import {
  BrowserRouter,
  Link,
  Outlet,
  Route,
  RouterProvider,
  Routes,
  createBrowserRouter,
  useNavigate,
  useRoutes,
} from 'react-router-dom';
import Login from './views/login/Login';
import Test from './views/test/Test';
import router from './router';
function App() {
  // const userStore = useSelector((state: RootState) => state.user);
  // const dispath: RootDispath = useDispatch();
  // const changeMyName = () => {
  //   dispath(changeName("ardeu"));
  // };
  // const readyToRender = list.map((item, index) => (
  //   <div key={index}>{item.title}</div>
  // ));

  // //在action中获取数据
  // const getListByStore = () => {
  //   dispath(userQuery(1));
  // };

  return (
    <>
      {/* 
        {userStore.name}
        <button onClick={changeMyName}>点击修改数据</button>
        <button onClick={getListByStore}>在redux中获取数据</button>
      {userStore.users.map((item, index) => (
        <div key={index}>{item.title}</div>
      ))}

      <SyncTest></SyncTest> */}
      <RouterProvider router={router}></RouterProvider>
    </>
  );
}

export default App;
