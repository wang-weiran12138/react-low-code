import Mock from 'mockjs'

const random = Mock.Random;
Mock.mock('/api/test','get',()=>{
    return {
        error:0,
        data:{
            name:`alex${Date.now()}`,
            adress:random.cparagraph(12,22)
        }
    }
})