import { FunctionComponent, useEffect, useRef } from 'react';
import { Form, Input, Checkbox, Select } from 'antd';
import { QuestionTitlePropsType } from './interface';
import useQuestionStore from '../../../zustandStore/questionStore';
const PropComponent: FunctionComponent<QuestionTitlePropsType> = (props: QuestionTitlePropsType) => {
  const setComponents = useQuestionStore((state) => state.setComponents);
  const { isCenter, level, text, disabled } = props;
  const [form] = Form.useForm();
  //监听切换组件重新给form赋值
  useEffect(() => {
    form.setFieldsValue({ isCenter, level, text });
  }, [isCenter, level, text]);
  //改变form字段的值触发整个组件进行更新
  const hanlerValueChange = (changedValues: Record<string, any>) => {
    setComponents(changedValues);
  };
  return (
    <Form
      disabled={disabled}
      onValuesChange={hanlerValueChange}
      form={form}
      layout="vertical"
      initialValues={{ isCenter, level, text }}>
      <Form.Item label="标题内容" name="text" rules={[{ required: true, message: '请输入标题内容' }]}>
        <Input />
      </Form.Item>
      <Form.Item label="层级" name="level">
        <Select
          options={[
            { value: 1, label: 1 },
            { value: 2, label: 2 },
            { value: 3, label: 3 },
          ]}
        />
      </Form.Item>
      <Form.Item name="isCenter" valuePropName="checked">
        <Checkbox>居中显示</Checkbox>
      </Form.Item>
    </Form>
  );
};

export default PropComponent;
