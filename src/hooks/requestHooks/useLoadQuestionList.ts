import { useRequest } from 'ahooks';
import { useSearchParams } from 'react-router-dom';
import { getQuestionList } from '../../api/question';
type OptionType = {
  isStar: boolean;
  isDeleted: boolean;
};
function useLoadQuestionList(options: Partial<OptionType> = {}) {
  const { isStar, isDeleted } = options;
  const [searchParams] = useSearchParams();
  const { data, error, loading, refresh } = useRequest(
    async () => {
      const keyword = searchParams.get('search') || '';
      const page = Number(searchParams.get('page')) || 1;
      const pageSize = Number(searchParams.get('pageSize')) || 10;
      const data = await getQuestionList({
        keyword,
        isStar,
        isDeleted,
        page,
        pageSize,
      });
      return data;
    },
    {
      refreshDeps: [searchParams],
    },
  );
  return { data, error, loading, refresh };
}
export default useLoadQuestionList;
