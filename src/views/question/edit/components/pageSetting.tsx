import { Form, Input } from 'antd';
import { FunctionComponent } from 'react';
import useGetPageInfo from '../../../../hooks/useGetPageInfo';
import TextArea from 'antd/es/input/TextArea';

const PageSetting: FunctionComponent = () => {
  const { css, description, js, setPageInfo, title } = useGetPageInfo();
  return (
    <Form
      initialValues={{ title, description, js, css }}
      onValuesChange={(value) => setPageInfo(value)}
      layout="vertical">
      <Form.Item name="title" label="页面标题" rules={[{ required: true, message: '页面标题不能为空！' }]}>
        <Input placeholder="请输入页面标题" />
      </Form.Item>
      <Form.Item name="description" label="页面描述">
        <TextArea />
      </Form.Item>
      <Form.Item name="js" label="js脚本设置">
        <TextArea />
      </Form.Item>
      <Form.Item name="css" label="css脚本设置">
        <TextArea />
      </Form.Item>
    </Form>
  );
};

export default PageSetting;
