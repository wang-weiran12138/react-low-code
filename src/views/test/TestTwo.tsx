import { FunctionComponent } from 'react';
import Container from './contaioner';

interface TestTwoProps {}

const TestTwo: FunctionComponent<TestTwoProps> = () => {
  return (
    <>
      <p>dnd-kit demo</p>
      <Container></Container>
    </>
  );
};

export default TestTwo;
