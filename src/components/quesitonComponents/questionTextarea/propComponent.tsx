import { FunctionComponent, useEffect } from 'react';
import { Form, Input } from 'antd';
import { QuestionTextareaPropsType } from './interface';
import useQuestionStore from '../../../zustandStore/questionStore';

const PropComponent: FunctionComponent<QuestionTextareaPropsType> = (
  props: QuestionTextareaPropsType,
) => {
  const setComponents = useQuestionStore(state => state.setComponents);
  const { placeholder, title, disabled = false } = props;
  const [form] = Form.useForm();
  //监听切换组件重新给form赋值
  useEffect(() => {
    form.setFieldsValue({ placeholder, title });
  }, [placeholder, title]);
  //改变form字段的值触发整个组件进行更新
  const hanlerValueChange = (changedValues: Record<string, any>) => {
    setComponents(changedValues);
  };
  return (
    <Form
      disabled={disabled}
      form={form}
      onValuesChange={hanlerValueChange}
      layout="vertical"
      initialValues={{ placeholder, title }}
    >
      <Form.Item
        label="标题"
        name="title"
        rules={[{ required: true, message: '请输入标题' }]}
      >
        <Input value={title} />
      </Form.Item>
      <Form.Item label="placeholder" name="placeholder">
        <Input.TextArea />
      </Form.Item>
    </Form>
  );
};

export default PropComponent;
