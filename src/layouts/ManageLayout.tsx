import { FunctionComponent, useRef } from 'react';
import { Outlet, useLocation, useNavigate, useOutlet } from 'react-router-dom';
import styles from './ManageLayout.module.scss';
import { Button, Space, Divider } from 'antd';
import { createQuestion } from '../api/question';
import { DeleteOutlined, PlusOutlined, StarOutlined, FrownOutlined } from '@ant-design/icons';
import useRequest from '../hooks/useRequest';
import ActiveRouter from './ActiveRouter';
const ManageLayout: FunctionComponent = () => {
  const nav = useNavigate();
  const { pathname } = useLocation();
  const outlet = useOutlet();
  const {
    loading: questionLoading,
    data: questionState,
    fetchData,
  } = useRequest(createQuestion, {
    onSuccess(res: any) {
      nav(`/question/edit/${res}`);
    },
  });

  //创建问卷
  const hanlderCreatBtn = async () => {
    await fetchData();
  };

  return (
    <>
      <div className={styles.container}>
        <div className={styles.left}>
          <Space direction="vertical">
            <Button
              onClick={hanlderCreatBtn}
              type="primary"
              size={'large'}
              icon={<PlusOutlined />}
              loading={questionLoading}>
              创建问卷{questionState}
            </Button>
            <Divider />
            {/* <MenuList /> */}
            <Button
              onClick={() => {
                nav('/manager/list');
              }}
              type={pathname.startsWith('/manager/list') ? 'default' : 'text'}
              icon={<FrownOutlined />}>
              我的问卷
            </Button>
            <Button
              onClick={() => {
                nav('/manager/star');
              }}
              type={pathname.startsWith('/manager/star') ? 'default' : 'text'}
              icon={<StarOutlined />}>
              星标问卷
            </Button>
            <Button
              onClick={() => {
                nav('/manager/trash');
              }}
              type={pathname.startsWith('/manager/trash') ? 'default' : 'text'}
              icon={<DeleteOutlined />}>
              回收站
            </Button>
          </Space>
        </div>
        <div className={styles.right}>
          {/** 组件持久化 */}
          {/* <ActiveRouter outlet={outlet} /> */}
          <Outlet />
        </div>
      </div>
    </>
  );
};

export default ManageLayout;
