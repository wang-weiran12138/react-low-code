import { useEffect, useState } from 'react';
const useMouse = () => {
  const [x, setX] = useState<number>();
  const [y, setY] = useState<number>();
  const mouseHandler = (event: MouseEvent) => {
    setX(event.clientX);
    setY(event.clientY);
  };
  useEffect(() => {
    window.addEventListener('mousemove', mouseHandler);
    return () => {
      window.removeEventListener('mousemove', mouseHandler);
    };
  }, []);
  return [x, y];
};

export default useMouse;
