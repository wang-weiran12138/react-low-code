import { FunctionComponent, useEffect } from 'react';
import { Form, Input, Checkbox } from 'antd';
import { QuestionParagraphPropsType } from './interface';
import useQuestionStore from '../../../zustandStore/questionStore';
const { TextArea } = Input;
const PropComponent: FunctionComponent<QuestionParagraphPropsType> = (
  props: QuestionParagraphPropsType,
) => {
  const setComponents = useQuestionStore(state => state.setComponents);
  const { disabled, isCenter, text } = props;
  const [form] = Form.useForm();
  //监听切换组件重新给form赋值
  useEffect(() => {
    form.setFieldsValue({ isCenter, text });
  }, [isCenter, text]);
  //改变form字段的值触发整个组件进行更新
  const hanlerValueChange = (changedValues: Record<string, any>) => {
    setComponents(changedValues);
  };
  return (
    <Form
      onValuesChange={hanlerValueChange}
      form={form}
      disabled={disabled}
      layout="vertical"
      initialValues={{ isCenter, text }}
    >
      <Form.Item
        label="段落内容"
        name="text"
        rules={[{ required: true, message: '请输入段落内容' }]}
      >
        <TextArea />
      </Form.Item>
      <Form.Item name="isCenter" valuePropName="checked">
        <Checkbox>居中显示</Checkbox>
      </Form.Item>
    </Form>
  );
};

export default PropComponent;
