import { questionInputConf } from './questionInput';
import { questionTitleConf } from './questionTitle';
import { QuestionInputPropsType } from './questionInput/interface';
import { QuestionTitlePropsType } from './questionTitle/interface';
import { FC } from 'react';
import { questionParagraphConf } from './questionParagraph';
import { QuestionParagraphPropsType } from './questionParagraph/interface';
import { questionInfoConf } from './questionInfo';
import { QuestionInfoPropsType } from './questionInfo/interface';
import { questionTextareaConf } from './questionTextarea';
import { QuestionTextareaPropsType } from './questionTextarea/interface';
import { questionRadioConf } from './questionRadio';
import { QuestionRadioPropsType } from './questionRadio/interface';
import { QuestionCheckboxPropsType } from './questionCheckbox/interface';
import { questionCheckboxConf } from './questionCheckbox';
export type ComponentPropsType = QuestionInputPropsType &
  QuestionTitlePropsType &
  QuestionParagraphPropsType &
  QuestionInfoPropsType &
  QuestionTextareaPropsType &
  QuestionRadioPropsType &
  QuestionCheckboxPropsType;
export type ComponentConfType = {
  type: string;
  Component: FC<ComponentPropsType>;
  title: string;
  defaultProps: Record<string, any>;
  PropComponent: FC<ComponentPropsType>;
};
export const questionConfs: ComponentConfType[] = [
  questionInfoConf,
  questionInputConf,
  questionTitleConf,
  questionParagraphConf,
  questionTextareaConf,
  questionRadioConf,
  questionCheckboxConf,
];
//组件分组
export const componentConfGroup = [
  {
    groupName: '文本显示',
    components: [questionInfoConf, questionTitleConf, questionParagraphConf],
  },
  {
    groupName: '用户输入',
    components: [questionInputConf, questionTextareaConf],
  },
  {
    groupName: '用户选择',
    components: [questionRadioConf, questionCheckboxConf],
  },
];

//根据传入的type来判断返回哪一个组件
export function getComponentByType(type: string) {
  return questionConfs.find((c) => c.type === type);
}
