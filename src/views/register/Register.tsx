import { FunctionComponent } from 'react';
import styles from './Register.module.scss';
import { Button, Form, Input, message } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { randomName } from '../../utils/common';
import { Link, useNavigate } from 'react-router-dom';
import { registerService } from '../../api/user';
import { setStorage } from '../../utils/storage';
import { useRequest } from 'ahooks';
const Register: FunctionComponent = () => {
  const nav = useNavigate();
  const [form] = useForm();
  const { loading, runAsync } = useRequest(registerService, { manual: true });
  const submitFn = async (value: any) => {
    await runAsync(value);
    message.success('注册成功');
    setStorage('login', { username: value.username, password: value.password });
    nav('/login');
  };
  const resetFn = () => {
    form.resetFields();
  };
  const randomNameFn = () => {
    form.setFieldValue('name', randomName('用户', 3));
  };
  return (
    <>
      <div className={styles.container}>
        <div className={styles.main}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <h3>注册新用户</h3>
            <Link to={{ pathname: '/login' }}>已有用户，去登录</Link>
          </div>
          <Form
            form={form}
            labelCol={{ span: 4 }}
            style={{ width: '500px' }}
            labelAlign="left"
            onFinish={submitFn}
          >
            <Form.Item
              label="用户名"
              name="username"
              rules={[{ required: true, message: '用户名不能为空！' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="密码"
              name="password"
              rules={[{ required: true, message: '密码不能为空！' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              label="确认密码"
              name="password_confirm"
              dependencies={['password']} //依赖项，password改变之后会重新触发校验
              rules={[
                { required: true, message: '不能为空！' },
                ({ getFieldValue }) => {
                  return {
                    validator(_, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      } else {
                        return Promise.reject(new Error('两次密码不一致'));
                      }
                    },
                  };
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              shouldUpdate={(prevValues, currentValues) =>
                currentValues.name != prevValues.name
              }
            >
              {({ getFieldValue }) =>
                getFieldValue('name') === 'alex' ? (
                  <Form.Item label="自定义" name="zidingyi">
                    <Input />
                  </Form.Item>
                ) : (
                  ''
                )
              }
            </Form.Item>
            <Form.Item
            //   wrapperCol={{
            //     offset: 12,
            //     span: 8,
            //   }}
            >
              <Button
                type="primary"
                htmlType="submit"
                style={{
                  margin: '0 auto',
                  width: '50%',
                  display: 'block',
                }}
                loading={loading}
              >
                提交
              </Button>
            </Form.Item>
            <Form.Item
            //   wrapperCol={{
            //     offset: 12,
            //     span: 8,
            //   }}
            >
              <Button
                style={{
                  margin: '0 auto',
                  width: '50%',
                  display: 'block',
                }}
                onClick={resetFn}
              >
                重置
              </Button>
            </Form.Item>
            <Form.Item
            //   wrapperCol={{
            //     offset: 12,
            //     span: 8,
            //   }}
            >
              <Button
                style={{
                  margin: '0 auto',
                  width: '50%',
                  display: 'block',
                }}
                onClick={randomNameFn}
              >
                随机姓名
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
};

export default Register;
