import { FunctionComponent } from 'react';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
interface ItemProps {
  id: string;
  title: string;
}

const Item: FunctionComponent<ItemProps> = (props: ItemProps) => {
  const { id, title } = props;
  const { attributes, listeners, setNodeRef, transform, transition } = useSortable({ id });
  const style = {
    transform: CSS.Transform.toString(transform),
    border: '1px solid #ccc',
    transition,
    margin: '10px 0',
    backgroundColor: '#f1f1f1',
  };
  return (
    <div ref={setNodeRef} style={style} {...attributes} {...listeners}>
      Item {title}
    </div>
  );
};

export default Item;
