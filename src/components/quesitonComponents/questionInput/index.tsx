import { FunctionComponent } from 'react';
import { Input, Typography } from 'antd';
import { QuestionInputPropsType } from './interface';
import PropComponent from './propComponent';
const QuestionInput: FunctionComponent<QuestionInputPropsType> = (props: QuestionInputPropsType) => {
  const { placeholder = '请输入内容', title = '输入框', className } = props;
  return (
    <>
      <div className={className}>
        <Typography.Paragraph strong>{title}</Typography.Paragraph>
        <div style={{}}>
          <Input placeholder={placeholder} />
        </div>
      </div>
    </>
  );
};
//组件配置
export const questionInputConf = {
  type: 'questionInput',
  title: '输入框',
  Component: QuestionInput,
  PropComponent,
  defaultProps: { placeholder: '请输入内容', title: '输入框' },
};
export default QuestionInput;
