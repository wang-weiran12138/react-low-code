import { FunctionComponent } from 'react';
import { Outlet } from 'react-router-dom';

const QuestionLayout: FunctionComponent = () => {
  return (
    <>
      <div>
        <Outlet></Outlet>
      </div>
    </>
  );
};

export default QuestionLayout;
