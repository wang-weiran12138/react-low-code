import { FunctionComponent, useEffect, useState } from 'react';
import QuestionCard from '../../../components/questionCard/questionCard';
import styles from './list.module.scss';
import ListSearch from '../../../components/listSearch/ListSearch';
import useRequest from '../../../hooks/useRequest';
import { getQuestionList } from '../../../api/question';
import { Spin, Empty } from 'antd';
import { useSearchParams } from 'react-router-dom';
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ListProps {}

const List: FunctionComponent<ListProps> = () => {
  const [searchParams] = useSearchParams();
  const [page, setPage] = useState(1);
  const [pageSize] = useState(10);
  const {
    data: questionData,
    loading,
    refresh,
  } = useRequest(
    () =>
      getQuestionList({
        keyword: searchParams.get('search') || '',
        page,
        pageSize,
        isDeleted: false,
      }),
    { immediate: true, refreshDeps: [searchParams.get('search')] },
    // {immediate:true}
  );
  //删除回调
  const onDelete = () => {
    // refresh();
  };
  //渲染list数据的函数
  const renderQuestionList = (questionList: any) => {
    if (questionList && questionList.length > 0) {
      return questionList.map((item: any) => <QuestionCard key={item.id} {...item} onDelete={onDelete}></QuestionCard>);
    } else if (!loading && questionData?.data?.length <= 0) {
      return (
        <div>
          <Empty description="暂无更多数据" />
        </div>
      );
    }
  };

  //滚动加载
  const hanlderScroll = (e: any) => {
    if (loading || (questionData && questionData.data.length <= 0)) return;
    if (e.target.scrollTop + e.target.clientHeight >= e.target.scrollHeight) {
      return setPage((val) => val + 1);
    }
  };
  useEffect(() => {
    if ((page - 1) * pageSize < questionData.total) {
      page == 1 ? refresh() : refresh({ resType: 'add' });
    }
  }, [page]);
  //当search改变时，page重置为1
  useEffect(() => {
    setPage(1);
  }, [searchParams.get('search')]);
  return (
    <>
      <div className={styles.container}>
        <div className={styles.header}>
          <div className={styles.left}>
            <h3>我的问卷</h3>
          </div>
          <div className={styles.right}>
            <ListSearch />
          </div>
        </div>
        <div className={styles.main} onScrollCapture={hanlderScroll}>
          {/* {questionList && questionList.length>0
            ? questionList.map((item:any) => (
                <QuestionCard key={item.id} {...item}></QuestionCard>
              ))
            : "暂无数据"} */}
          {/* {loading?<div style={{textAlign:'center'}}><Spin /></div>:renderQuestionList(questionList)} */}
          {/* {<div style={{margin:'0 auto'}}><Spin/></div>} 不能用 因为spin是inline-block */}
          {renderQuestionList(questionData.data)}
          {loading ? (
            <div style={{ textAlign: 'center' }}>
              <Spin></Spin>
            </div>
          ) : (
            '暂无更多数据'
          )}
        </div>
      </div>
    </>
  );
};

export default List;
