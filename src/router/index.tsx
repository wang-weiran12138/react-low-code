import { Navigate, createBrowserRouter } from 'react-router-dom';
import MainLayout from '../layouts/MainLayout';
// import ManageLayout from '../layouts/ManageLayout';
import QuestionLayout from '../layouts/QuestionLayout';
import Login from '../views/login/Login';
import Register from '../views/register/Register';
import List from '../views/manage/list/list';
import Trash from '../views/manage/trash/Trash';
import Star from '../views/manage/star/Star';
import Stat from '../views/question/stat';
import Edit from '../views/question/edit';
import NotFound from '../views/404/NotFound';
import Test from '../views/test/Test';
import RequireAuth from './authGuard';
import TestTwo from '../views/test/TestTwo';
import { Suspense, lazy } from 'react';
//懒加载
const Home = lazy(() => import('../views/home/Home'));
const ManageLayout = lazy(() => import('../layouts/ManageLayout'));
const router = createBrowserRouter([
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        path: '/',
        element: (
          // <Suspense fallback={<h2>加载中...</h2>}>
          <Home />
          // </Suspense>
        ),
      },
      {
        path: 'login',
        element: <Login />,
      },
      {
        path: 'register',
        element: <Register />,
      },
      {
        path: '/manager',
        element: (
          <RequireAuth>
            <ManageLayout />
          </RequireAuth>
        ),

        children: [
          //重定向 index 为默认路径 replace确保不会留下记录
          {
            index: true,
            element: <Navigate to="list" replace />,
          },
          {
            path: 'list',
            element: (
              <RequireAuth>
                <List />
              </RequireAuth>
            ),
          },
          {
            path: 'star',
            element: (
              <RequireAuth>
                <Star />
              </RequireAuth>
            ),
          },
          {
            path: 'trash',
            element: (
              <RequireAuth>
                <Trash />
              </RequireAuth>
            ),
          },
        ],
      },
      {
        path: '*',
        element: <NotFound />,
      },
    ],
  },
  {
    path: '/question',
    element: (
      <RequireAuth>
        <QuestionLayout />
      </RequireAuth>
    ),
    children: [
      {
        index: true,
        element: <Navigate to="stat" replace />,
      },
      {
        path: 'stat/:id',
        element: (
          <RequireAuth>
            <Stat />
          </RequireAuth>
        ),
      },
      {
        path: 'edit/:id',
        element: (
          <RequireAuth>
            <Edit />
          </RequireAuth>
        ),
      },
    ],
  },
  {
    path: '/test',
    element: <Test />,
  },
  {
    path: '/test2',
    element: <TestTwo />,
  },
]);
export default router;
