import { FunctionComponent, useEffect } from 'react';
import styles from './Login.module.scss';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { useForm } from 'antd/es/form/Form';
import {
  Link,
  useLocation,
  useNavigate,
  useNavigation,
} from 'react-router-dom';
import { getStorage, removeStorage, setStorage } from '../../utils/storage';
import { loginService } from '../../api/user';
import { useRequest } from 'ahooks';
import useStore from '../../zustandStore/index';
const Login: FunctionComponent = () => {
  const setToken = useStore(state => state.setToken);
  const { username } = useStore(state => state.userInfo);
  const nav = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || '/';
  const [form] = useForm();
  const { loading, runAsync } = useRequest(loginService, { manual: true });
  const submitFn = async (value: any) => {
    const loginInfo = {
      username: value.username,
      password: value.password,
    };
    const { data } = await runAsync(loginInfo);
    setStorage('token', data?.token);
    setToken(data?.token);
    if (value.remberme) {
      setStorage('login', loginInfo);
    } else {
      removeStorage('login');
    }
    message.success('登录成功');
    nav({ pathname: '/manager/list' });
  };
  useEffect(() => {
    if (username) nav(from, { replace: true });
    if (getStorage('login')) {
      const loginInfo = JSON.parse(getStorage('login')!);
      loginInfo &&
        form.setFieldsValue({
          username: loginInfo.username,
          password: loginInfo.password,
        });
    }
  }, []);
  return (
    <>
      <div className={styles.container}>
        <div className={styles.main}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <h3>登录</h3>
            <Link to={{ pathname: '/register' }}>没有用户，去注册</Link>
          </div>
          <Form
            form={form}
            labelCol={{ span: 4 }}
            style={{ width: '500px' }}
            labelAlign="left"
            // initialValues={{name:'alex'}}
            onFinish={submitFn}
          >
            <Form.Item
              label="用户名"
              name="username"
              rules={[{ required: true, message: '用户名不能为空！' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="密码"
              name="password"
              rules={[{ required: true, message: '密码不能为空！' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="remberme"
              valuePropName="checked"
              initialValue={true}
            >
              <Checkbox>记住我</Checkbox>
            </Form.Item>
            <Form.Item
              shouldUpdate={(prevValues, currentValues) =>
                currentValues.name != prevValues.name
              }
            >
              {({ getFieldValue }) =>
                getFieldValue('name') === 'alex2' ? (
                  <Form.Item label="自定义" name="zidingyi">
                    <Input />
                  </Form.Item>
                ) : (
                  ''
                )
              }
            </Form.Item>
            <Form.Item
            //   wrapperCol={{
            //     offset: 12,
            //     span: 8,
            //   }}
            >
              <Button
                type="primary"
                htmlType="submit"
                style={{
                  margin: '0 auto',
                  width: '50%',
                  display: 'block',
                }}
                loading={loading}
              >
                登录
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
};

export default Login;
