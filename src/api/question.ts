import axiosInstance from '../utils/request';
//创建问卷
export const createQuestion = (data?: any): Promise<number> => {
  return axiosInstance({ url: `/question`, method: 'post', data });
};
//获取问卷详情
export const getQuestion = (id: number): Promise<Record<string, any>> => {
  return axiosInstance({ url: `/question/${id}`, method: 'get' });
};
//获取问卷列表
type SearchOption = {
  keyword: string;
  page: number;
  pageSize: number;
  sort: string;
  isStar: boolean;
  isDeleted: boolean;
};
export const getQuestionList = (searchOption: Partial<SearchOption>): Promise<any> => {
  return axiosInstance({
    url: `/question`,
    method: 'get',
    params: searchOption,
  });
};
// 更新问卷
export const updateQuestion = (
  id: number | string | (number | string)[],
  data?: { [key: string]: any },
): Promise<any> => {
  return Array.isArray(id)
    ? axiosInstance({
        url: `/question/batch`,
        method: 'patch',
        data: { ids: id, ...data },
      })
    : axiosInstance({ url: `/question/${id}`, method: 'patch', data });
};
//复制问卷
export const duplicateQuestion = (id: number | string): Promise<any> => {
  return axiosInstance({ url: `/question/duplicate/${id}`, method: 'post' });
};
//创建模拟问卷数据
export const createMockQuestion = (len: number): Promise<any> => {
  return axiosInstance({
    url: `/question/mock/`,
    method: 'post',
    data: { len },
  });
};
