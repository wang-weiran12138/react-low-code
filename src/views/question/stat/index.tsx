import { FunctionComponent } from 'react';
import { useParams } from 'react-router';
import { useRequest } from 'ahooks';
import { getQuestion } from '../../../api/question';
import EditHeader from './components/editHeader';

const Stat: FunctionComponent = () => {
  const { id = '' } = useParams();
  const { loading: editInfoLoading, data: editInfo } = useRequest(() => getQuestion(+id!));
  return (
    <>
      <EditHeader />
    </>
  );
};

export default Stat;
