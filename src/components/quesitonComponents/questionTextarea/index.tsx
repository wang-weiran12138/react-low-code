import { FunctionComponent } from 'react';
import { Input, Typography } from 'antd';
import { QuestionTextareaPropsType } from './interface';
import PropComponent from './propComponent';
const QuestionTextarea: FunctionComponent<QuestionTextareaPropsType> = (props: QuestionTextareaPropsType) => {
  const { placeholder = '请输入内容', title = '输入框', className } = props;
  return (
    <>
      <div className={className}>
        <Typography.Paragraph strong>{title}</Typography.Paragraph>
        <div style={{}}>
          <Input.TextArea placeholder={placeholder} />
        </div>
      </div>
    </>
  );
};
//组件配置
export const questionTextareaConf = {
  type: 'questionTextarea',
  Component: QuestionTextarea,
  title: '文本框',
  PropComponent,
  defaultProps: { placeholder: '请输入内容', title: '输入框' },
};
export default QuestionTextarea;
