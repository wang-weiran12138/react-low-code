export type QuestionTextareaPropsType = {
  title?: string;
  placeholder?: string;
  className?: string;
  disabled?: boolean;
};
