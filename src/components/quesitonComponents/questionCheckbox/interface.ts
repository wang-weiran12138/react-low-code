export type OptionType = {
  value: string;
  text: string;
  checked: boolean;
};
export type QuestionCheckboxPropsType = {
  title?: string;
  isVertical?: boolean;
  list?: OptionType[];
  className?: string;
  disabled?: boolean;
};

export const QuestionCheckboxDefaultProps: QuestionCheckboxPropsType = {
  title: '多选标题',
  isVertical: false,
  list: [
    { value: '1', text: '选项1', checked: true },
    { value: '2', text: '选项2', checked: true },
    { value: '3', text: '选项3', checked: false },
  ],
};
