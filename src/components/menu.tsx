// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore

import {Link,NavLink} from 'react-router-dom'
import './menu.css'
function Menu() {
  return (
    <div>
      {/* <Link to='/'>主页</Link> */}
      <Link to='/about/1'>about</Link>
       
      

      {/* <NavLink exact activeClassName="active" to="/">访问主页</NavLink> */}
      <NavLink exact activeStyle={{textDecoration:'underline'}} to="/">访问主页</NavLink>
    </div>
  );
}

export default Menu;
