import { FunctionComponent, useEffect } from 'react';
import { Form, Input } from 'antd';
import { QuestionInfoPropsType } from './interface';
import useQuestionStore from '../../../zustandStore/questionStore';
const { TextArea } = Input;
const PropComponent: FunctionComponent<QuestionInfoPropsType> = (props: QuestionInfoPropsType) => {
  const setComponents = useQuestionStore((state) => state.setComponents);
  const { disabled, title, text } = props;
  const [form] = Form.useForm();
  //监听切换组件重新给form赋值
  useEffect(() => {
    form.setFieldsValue({ title, text });
  }, [title, text]);
  //改变form字段的值触发整个组件进行更新
  const hanlerValueChange = (changedValues: Record<string, any>) => {
    setComponents(changedValues);
  };
  return (
    <Form
      onValuesChange={hanlerValueChange}
      form={form}
      disabled={disabled}
      layout="vertical"
      initialValues={{ title, text }}>
      <Form.Item name="title" label="问卷标题" rules={[{ required: true, message: '请输入段落内容' }]}>
        <Input />
      </Form.Item>
      <Form.Item label="问卷描述" name="text">
        <TextArea />
      </Form.Item>
    </Form>
  );
};

export default PropComponent;
