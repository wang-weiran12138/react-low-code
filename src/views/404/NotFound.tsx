import { FunctionComponent } from "react";
import { Button, Result } from 'antd';
import { useNavigate } from "react-router-dom";
interface NotFoundProps {
    
}
 
const NotFound: FunctionComponent<NotFoundProps> = () => {
    const nav = useNavigate()
    return (  <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={<Button type="primary" onClick={()=>nav(-1)}>返回</Button>}
      /> );
}
 
export default NotFound;