import { FunctionComponent } from 'react';
import { QuestionInfoPropsType } from './interface';
import { Typography } from 'antd';
import PropComponent from './propComponent';
const { Paragraph } = Typography;
const QuestionInfo: FunctionComponent<QuestionInfoPropsType> = (props: QuestionInfoPropsType) => {
  const { text = '', title = '' } = props;
  const textList = text?.split('\n');
  return (
    <>
      <Typography.Title level={4} style={{ textAlign: 'center' }}>
        {title}
      </Typography.Title>
      <Paragraph style={{ textAlign: 'center' }}>
        {textList.map((t, index) => (
          <span key={index}>
            {index > 0 && <br />}
            {t}
          </span>
        ))}
      </Paragraph>
    </>
  );
};
const defaultProps: QuestionInfoPropsType = {
  text: '问卷描述...',
  title: '问卷标题',
};
export const questionInfoConf = {
  type: 'questionInfo',
  title: '段落',
  Component: QuestionInfo,
  PropComponent,
  defaultProps,
};
export default QuestionInfo;
