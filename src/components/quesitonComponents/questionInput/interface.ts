export type QuestionInputPropsType = {
  title?: string;
  placeholder?: string;
  className?: string;
  disabled?: boolean;
};
