import { FunctionComponent, useState } from 'react';
import { useGetComponentInfo } from '../../../../hooks/useGetComponentInfo';
import { EyeInvisibleOutlined, LockOutlined } from '@ant-design/icons';
import styles from './layers.module.scss';
import { Button, Input, Space } from 'antd';
import SortableContainer from '../../../../components/dragSortable/SortableContainer';
import SortableItem from '../../../../components/dragSortable/sortableItem';
const Layers: FunctionComponent = () => {
  const [isSelectAgainId, setIsSelectAgainId] = useState('');
  const [inputValue, setInputValue] = useState('');
  const { moveComponent } = useGetComponentInfo();
  const {
    components,
    selectedId,
    setComponentInfo,
    changeSelectedId,
    LockorUnlockSelectedComponent,
    hideOrShowSelectedComponentOnLayers,
  } = useGetComponentInfo();
  //点击一次选中，二次切换为input
  const selectCell = (id: string, val: string) => {
    if (id != selectedId) {
      changeSelectedId(id);
      setIsSelectAgainId('');
      return;
    }
    setIsSelectAgainId(id);
    setInputValue(val);
  };
  //输入改变input的值
  const changeInputValue = (val: string) => {
    setInputValue(val.trim());
  };
  //回车或者blur同步到store
  const confirmValue = () => {
    setComponentInfo({ title: inputValue });
    setIsSelectAgainId('');
    setInputValue('');
  };
  const hanlderHidden = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, id: string) => {
    e.stopPropagation();
    changeSelectedId(id);
    hideOrShowSelectedComponentOnLayers(id);
    setIsSelectAgainId('');
  };
  const hanlderLock = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, id: string) => {
    e.stopPropagation();
    changeSelectedId(id);
    LockorUnlockSelectedComponent(id);
    setIsSelectAgainId('');
  };
  //处理拖拽事件
  const hanlderDragEnd = (oldIndex: number, newIndex: number) => {
    moveComponent(oldIndex, newIndex);
  };
  return (
    <SortableContainer items={components} onDragEnd={hanlderDragEnd}>
      {components.map((component, index) => {
        return (
          <SortableItem id={component.id} key={component.id}>
            <div
              onClick={() => selectCell(component.id, component.title)}
              key={index}
              className={`${styles['cell-wrapper']} ${selectedId === component.id ? styles.active : ''}`}>
              {isSelectAgainId === component.id && (
                <Input
                  value={inputValue}
                  onChange={(e) => {
                    changeInputValue(e.target.value);
                  }}
                  onPressEnter={confirmValue}
                  onBlur={confirmValue}
                />
              )}
              {!(isSelectAgainId === component.id) && (
                <span style={{ color: selectedId === component.id ? 'skyblue' : '#000' }}>{component.title}</span>
              )}
              <div className={styles.icons}>
                <Space>
                  <Button
                    className={component.isHidden ? '' : styles.btn}
                    type={component.isHidden ? 'primary' : 'default'}
                    size="small">
                    <EyeInvisibleOutlined onClick={(e) => hanlderHidden(e, component.id)} />
                  </Button>
                  <Button
                    className={component.isLocked ? '' : styles.btn}
                    type={component.isLocked ? 'primary' : 'default'}
                    size="small">
                    <LockOutlined onClick={(e) => hanlderLock(e, component.id)} />
                  </Button>
                </Space>
              </div>
            </div>
          </SortableItem>
        );
      })}
    </SortableContainer>
  );
};

export default Layers;
