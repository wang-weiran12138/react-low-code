import { FunctionComponent, Suspense } from 'react';
import { Outlet } from 'react-router-dom';

import { Layout } from 'antd';
import styles from './MainLayout.module.scss';
import Logo from '../components/logo/Logo';
import UserInfo from '../components/userInfo/UserInfo';

const MainLayout: FunctionComponent = () => {
  const { Header, Content, Footer } = Layout;

  return (
    <Layout className={styles.container}>
      <Header className={styles.head}>
        <div>
          <Logo />
        </div>
        <div>
          <UserInfo />
        </div>
      </Header>
      <Content className={styles.main}>
        {/* 懒加载 */}
        <Suspense fallback={<h2>加载中...</h2>}>
          <Outlet />
        </Suspense>
      </Content>
      <Footer className={styles.footer}>问卷 &copy;2023 - present. Create by alex</Footer>
    </Layout>
  );
};

export default MainLayout;
