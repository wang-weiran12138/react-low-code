import { FunctionComponent } from 'react';
import { Typography } from 'antd';
import { QuestionTitleDefaultProps, QuestionTitlePropsType } from './interface';
import PropComponent from './propComponent';
const QuestionTitle: FunctionComponent<QuestionTitlePropsType> = (props: QuestionTitlePropsType) => {
  const { text, level = 1, isCenter, className } = { ...QuestionTitleDefaultProps, ...props };
  const genFontSize = (level: number) => {
    switch (level) {
      case 1:
        return '24px';
      case 2:
        return '20px';
      case 3:
        return '16px';
      default:
        return '16px';
    }
  };
  return (
    <div className={className}>
      <Typography.Title
        level={level}
        style={{
          textAlign: isCenter ? 'center' : 'start',
          marginBottom: '0',
          fontSize: genFontSize(level),
        }}>
        {text}
      </Typography.Title>
    </div>
  );
};
//组件配置
export const questionTitleConf = {
  type: 'questionTitle',
  title: '标题',
  Component: QuestionTitle,
  PropComponent,
  defaultProps: {
    text: '一级标题',
    level: 1,
    isCenter: false,
  },
};
export default QuestionTitle;
