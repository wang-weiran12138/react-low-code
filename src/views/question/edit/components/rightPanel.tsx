import { FunctionComponent } from 'react';
import { Tabs } from 'antd';
import { FileTextOutlined, SettingOutlined } from '@ant-design/icons';
import ComponentProp from './componentProp';
import PageSetting from './pageSetting';
const RightPanel: FunctionComponent = () => {
  const tabsItem = [
    {
      key: '1',
      label: <span>属性</span>,
      children: <ComponentProp></ComponentProp>,
      icon: <FileTextOutlined />,
    },
    {
      key: '2',
      label: <span>页面设置</span>,
      children: <PageSetting />,
      icon: <SettingOutlined />,
    },
  ];
  return <Tabs defaultActiveKey="1" items={tabsItem} />;
};

export default RightPanel;
