import { memo, useEffect, useState } from 'react';
import { useLocation } from 'react-router';

function ActiveRouter({ outlet }) {
  const { pathname } = useLocation();
  const [componentList, setComponentList] = useState(new Map());

  useEffect(() => {
    setComponentList((componentList) => {
      if (!componentList.has(pathname)) {
        componentList.set(pathname, outlet);
      }
      return new Map([...componentList.entries()]);
    });
  }, [outlet, pathname]);
  return (
    <>
      {[...componentList.entries()].map(([key, component]) => (
        <div key={key} style={{ display: key === pathname ? 'block' : 'none' }}>
          {component}
        </div>
      ))}
    </>
  );
}
export default memo(ActiveRouter);
