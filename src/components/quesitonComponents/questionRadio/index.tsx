import { FunctionComponent } from 'react';
import { QuestionRadioPropsType, QuestionRadioDefaultProps } from './interface';
import { Radio, Space, Typography } from 'antd';
import PropComponent from './propComponent';
const QuestionRadio: FunctionComponent<QuestionRadioPropsType> = (props: QuestionRadioPropsType) => {
  const { isVertical, options, title, value } = props;
  return (
    <>
      <Typography.Paragraph>{title}</Typography.Paragraph>
      <Radio.Group value={value}>
        <Space direction={isVertical ? 'vertical' : 'horizontal'}>
          {options?.map((o) => {
            const { text, value } = o;
            return (
              <Radio key={value} value={value}>
                {text}
              </Radio>
            );
          })}
        </Space>
      </Radio.Group>
    </>
  );
};

export const questionRadioConf = {
  type: 'questionRadio',
  title: '单选框',
  Component: QuestionRadio,
  PropComponent,
  defaultProps: QuestionRadioDefaultProps,
};
export default QuestionRadio;
