export type QuestionInfoPropsType = {
  text?: string;
  title?: string;
  className?: string;
  disabled?: boolean;
};
