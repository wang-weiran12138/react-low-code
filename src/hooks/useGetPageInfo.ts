import pageInfoStore from '../zustandStore/pageInfo';
const useGetPageInfo = () => {
  const { setPageInfo, title, css, description, js } = pageInfoStore();
  return { setPageInfo, title, css, description, js };
};
export default useGetPageInfo;
