import { FunctionComponent } from 'react';
import { QuestionParagraphPropsType } from './interface';
import { Typography } from 'antd';
import PropComponent from './propComponent';
const { Paragraph } = Typography;
const QuestionParagraph: FunctionComponent<QuestionParagraphPropsType> = (
  props: QuestionParagraphPropsType,
) => {
  const { text = '', isCenter } = props;
  const textList = text?.split('\n');
  return (
    <Paragraph
      style={{ textAlign: isCenter ? 'center' : 'start', marginBottom: 0 }}
    >
      {textList.map((t, index) => (
        <span key={index}>
          {index > 0 && <br />}
          {t}
        </span>
      ))}
    </Paragraph>
  );
};
const defaultProps: QuestionParagraphPropsType = {
  text: '这是一段段落',
  isCenter: false,
};
export const questionParagraphConf = {
  type: 'questionParagraph',
  title: '段落',
  Component: QuestionParagraph,
  PropComponent,
  defaultProps,
};
export default QuestionParagraph;
