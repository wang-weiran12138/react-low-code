import { FunctionComponent } from "react";
import { Space, Typography } from 'antd'
import { FormOutlined } from '@ant-design/icons'
import styles from './Logo.module.scss'
import { Link } from "react-router-dom";
interface LogoProps {

}
const { Title } = Typography
const Logo: FunctionComponent<LogoProps> = () => {

    return (
            <Link to={'/'}>
                <Space className={styles.container}>
                    <Title >
                        <FormOutlined />
                    </Title>
                    <Title>低代码问卷</Title>
                </Space>
            </Link>
    );
}

export default Logo;