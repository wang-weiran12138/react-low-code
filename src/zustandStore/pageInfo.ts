import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';
export type pageInfoType = {
  title: string;
  description?: string;
  js?: string;
  css?: string;
};
export type pageInfoStateType = pageInfoType & {
  setPageInfo: (value: pageInfoType) => void;
};
const pageInfoStore = create(
  persist<pageInfoStateType>(
    (set, get) => {
      return {
        title: '',
        description: '',
        js: '',
        css: '',
        setPageInfo(value) {
          set({ ...value });
        },
      };
    },
    {
      name: 'pageInfo', // 存储在localStorage中的key
      storage: createJSONStorage(() => localStorage), // 存储方式
    },
  ),
);
export default pageInfoStore;
