import { FunctionComponent, useEffect, useState } from "react";
import {Button,Typography} from 'antd'
import '../../_mock/index'
import style from './Home.module.scss'
import axiosInstance from "../../utils/request";
interface HomeProps {
    
}
const {Title,Paragraph} = Typography
const Home: FunctionComponent<HomeProps> = () => {
    const [name,setName] = useState<string>('')
    useEffect(()=>{
        axiosInstance.get('/auth/all',{headers:{
            Authorization:'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFsZXgyIiwic3ViIjozLCJpYXQiOjE3MDI2MzY3ODIsImV4cCI6MTcwMjk5Njc4Mn0.G7eg97pMvwcv63X0GOGlGGPu-WGg-w-6vHu3jHuZ95I'
        }}).then((res)=>console.log(res)).catch(err=>{console.log(err)})
    },[])
    return ( 
        <div className={`${style.container} ${style.container}`}>
            <Title>问卷调查 | 在线投票</Title>
            <Paragraph>已累计创建问卷1000份，发布问卷100份，收到答卷10000份</Paragraph>
            <Button>开始使用</Button>
        </div>
     );
}
 
export default Home;