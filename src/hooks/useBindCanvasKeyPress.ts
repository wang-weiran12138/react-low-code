import { useKeyPress } from 'ahooks';
import useQuestionStore from '../zustandStore/questionStore';

function isActiveElement() {
  const activeElem = document.activeElement;
  if (activeElem === document.body) return true;
  if (activeElem?.id === 'tag') return true;
  //dnd-kit会套一层div[role="button"]
  if (activeElem?.matches('div[role="button"]')) return true;
  return false;
}
function useBindCanvasKeyPress() {
  //如果在外部定义了store则会丢失响应性
  const store = useQuestionStore.getState();
  useKeyPress(['backspace', 'delete'], () => {
    if (!isActiveElement()) return;
    store.removeSelectedComponent(store.selectedId);
  });
  //复制
  useKeyPress(['ctrl.c'], () => {
    if (!isActiveElement()) return;
    store.copySelectedComponent(store.selectedId);
  });
  //粘贴
  useKeyPress(['ctrl.v'], () => {
    if (!isActiveElement()) return;
    store.PasteSelectedComponent();
  });
  //选中上一个
  useKeyPress(['uparrow'], () => {
    if (!isActiveElement()) return;
    console.log(1);
    store.selectPrevComponent();
  });
  //选中下一个
  useKeyPress(['downarrow'], () => {
    if (!isActiveElement()) return;
    store.selectNextComponent();
  });
}
export default useBindCanvasKeyPress;
