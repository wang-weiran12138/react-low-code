export type QuestionParagraphPropsType = {
  text?: string;
  isCenter?: boolean;
  className?: string;
  disabled?: boolean;
};
