import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import { Provider } from 'react-redux';
import store from './store';
import zhCN from 'antd/locale/zh_CN';
import { ConfigProvider } from 'antd';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-ignore

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <ConfigProvider locale={zhCN}>
      <App />
    </ConfigProvider>
  </Provider>,
);

/** */
