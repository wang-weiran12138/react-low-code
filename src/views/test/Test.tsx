import { FunctionComponent, useEffect, useState } from 'react';
import { json, useLocation, useNavigate, useParams, useSearchParams } from 'react-router-dom';
import { Button, Input } from 'antd';
import styles from './Test.module.scss';
import CustomTwo from './TestTwo';
import axiosInstance from '../../utils/request';
import { createMockQuestion } from '../../api/question';
import { getStorage } from '../../utils/storage';
import { testGetZustandByTs, testSetZustandByTs } from '../../zustandStore/tesst';
//测试zustand
import useStore from '../../zustandStore/index';
import { debounce } from 'lodash';
const Test: FunctionComponent = () => {
  const { userInfo } = useStore();
  const count = useStore((state: any) => state.count);
  const changnum = useStore((state: any) => state.changnum);
  const changeuser = useStore((state: any) => state.changeuser);

  const [searchParam, setSearchParam] = useSearchParams();
  const params = useParams();
  console.log(params);
  const { state } = useLocation();
  const navigate = useNavigate();
  const newInfo = {
    code: 0,
    msg: 'success',
    data: [
      { id: 4, title: '4' },
      { id: 5, title: '5' },
      { id: 6, title: '6' },
    ],
    total: 6,
    page: 2,
    pageSize: 3,
  };
  const [info, setInfo] = useState<any>({
    code: 0,
    msg: 'success',
    data: [
      { id: 1, title: '1' },
      { id: 2, title: '2' },
      { id: 3, title: '3' },
    ],
    total: 6,
    page: 1,
    pageSize: 3,
  });
  useEffect(() => {
    setTimeout(() => {
      setInfo({ ...newInfo, data: [...info.data, ...newInfo.data] });
    }, 1000);
  }, []);

  const age = searchParam.get('age') || state?.name || '';

  //创建模拟问卷数据
  const hanlderMockQuestionList = async () => {
    const res = await createMockQuestion(53);
    console.log(res);

    //测试class Decorator
    // const hanlderDecorator = () => {
    //   const TestDecorator = (target: any, propertyName: any) => {
    //     console.log(target, propertyName);
    //   };
    //   class Test {
    //     @TestDecorator
    //     name = 'alex';
    //   }
    // };
  };

  //测试防抖
  const hanlderDebouce = debounce(() => {
    console.log(123);
  }, 1000);

  //测试roles
  const hanlderRolesBtn = async () => {
    await axiosInstance({
      url: '/auth/6/roles',
      method: 'POST',
      data: { roleIds: [1, 2, 3] },
    });
  };

  //测试获取用户信息
  const hanlderUserInfo = () => {
    const token = getStorage('token') || '';
    axiosInstance.get('/auth/userInfo').then((res) => {
      console.log(res);
    });
  };
  return (
    <div>
      {JSON.stringify(info)}
      test
      {age && <div>{age}</div>}
      <button
        onClick={() => {
          setSearchParam('name=niger&age=21');
        }}>
        点击改变search数据
      </button>
      <button
        onClick={() => {
          navigate('/home/1');
        }}>
        变成导航2
      </button>
      <div className={`${styles.btn}`}>
        test
        <Button>antd</Button>
      </div>
      <div className={styles.wrapper}>
        <Input />
      </div>
      <CustomTwo className={styles.info}>123</CustomTwo>
      <Button onClick={hanlderMockQuestionList}>开始创建问卷列表模拟数据</Button>
      <Button onClick={hanlderRolesBtn}>测试roles</Button>
      <Button onClick={hanlderUserInfo}>获取用户信息</Button>
      <Button onClick={() => changeuser({ username: 'alex2' })}>测试zustand改用户信息1</Button>
      <Button onClick={() => changeuser({ nickname: 222 })}>测试zustand改用户信息2</Button>
      <Button onClick={() => changnum(12)}>测试zustand{count}</Button>
      <Button onClick={testGetZustandByTs}>测试zustand从Ts获取</Button>
      <Button onClick={() => testSetZustandByTs({ username: 'alex23' })}>测试zustand从ts更改</Button>
      <Button onClick={hanlderDebouce}>测试防抖</Button>
      <div>{userInfo.username}</div>
      <div>{userInfo.nickname}</div>
      <div>{userInfo.avatar}</div>
    </div>
  );
};

export default Test;
