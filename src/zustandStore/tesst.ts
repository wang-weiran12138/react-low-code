import useStore from './index';
export function testGetZustandByTs() {
  //   const userInfo = useStore(state => state.userInfo);
  // ↑无法在js/ts中使用，如果要使用则使用下面这个方法
  const userInfo = useStore.getState().userInfo;
  console.log(userInfo);
  return useStore;
}
export function testSetZustandByTs(param) {
  //在ts中使用仍然可以使用state
  useStore.setState(state => ({
    userInfo: { ...state.userInfo, ...param },
  }));
  return;
}
