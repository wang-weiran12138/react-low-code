export type OptionType = {
  value: string;
  text: string;
};
export type QuestionRadioPropsType = {
  title?: string;
  isVertical?: boolean;
  value?: string;
  options?: OptionType[];
  className?: string;
  disabled?: boolean;
};

export const QuestionRadioDefaultProps: QuestionRadioPropsType = {
  title: '单选标题',
  isVertical: false,
  value: '1',
  options: [
    { value: '1', text: '选项1' },
    { value: '2', text: '选项2' },
    { value: '3', text: '选项3' },
  ],
};
