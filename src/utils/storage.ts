export function setStorage(name: string, value: any, type = 'local'): void {
  const _value = JSON.stringify(value);
  return type == 'local'
    ? localStorage.setItem(name, _value)
    : sessionStorage.setItem(name, _value);
}
export function getStorage(name: string, type = 'local') {
  return type == 'local'
    ? localStorage.getItem(name)
    : sessionStorage.getItem(name);
}
export function removeStorage(name: string, type = 'local'): void {
  return type == 'local'
    ? localStorage.removeItem(name)
    : sessionStorage.removeItem(name);
}
