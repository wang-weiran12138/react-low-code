import { FunctionComponent } from 'react';
import { Tabs } from 'antd';
import { AppstoreAddOutlined, BarsOutlined } from '@ant-design/icons';
import ComponentsLib from './componentsLib';
import Layers from './layers';
interface LeftPanelProps {
  [key: string]: any;
}

const LeftPanel: FunctionComponent<LeftPanelProps> = () => {
  return (
    <>
      <Tabs
        defaultActiveKey="1"
        items={[
          {
            key: '1',
            label: <span>组件库</span>,
            children: (
              <div>
                <ComponentsLib />
              </div>
            ),
            icon: <AppstoreAddOutlined />,
          },
          {
            key: '2',
            label: <span>图层</span>,
            children: <Layers />,
            icon: <BarsOutlined />,
          },
        ]}
      />
    </>
  );
};

export default LeftPanel;
