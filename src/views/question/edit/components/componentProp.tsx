import { FunctionComponent } from 'react';
import useQuestionStore from '../../../../zustandStore/questionStore';
import { ComponentConfType, questionConfs } from '../../../../components/quesitonComponents';
const ComponentProp: FunctionComponent = () => {
  const components = useQuestionStore((state) => state.components);
  const selectedId = useQuestionStore((state) => state.selectedId);
  const selectedComponent = components.find((component) => component.id === selectedId);
  const getComponentProp = (c: ComponentConfType) => {
    if (c.type !== selectedComponent?.type) return;
    return (
      <div key={selectedComponent.id}>
        <c.PropComponent
          {...selectedComponent.props}
          disabled={selectedComponent.isLocked || selectedComponent.isHidden}
        />
      </div>
    );
  };
  return <>{selectedComponent ? questionConfs.map((c) => getComponentProp(c)) : <div>未选择</div>}</>;
};

export default ComponentProp;
