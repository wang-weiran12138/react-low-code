import { FunctionComponent, useEffect, useState } from 'react';
import { Input } from 'antd';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
const { Search } = Input;
const ListSearch: FunctionComponent = () => {
  const nav = useNavigate();
  const { pathname } = useLocation();
  const [searchParam] = useSearchParams();
  const [value, setValue] = useState<string>();
  //搜索相关method
  const onSearch = (value: string) => {
    nav(`${pathname}?search=${value}`);
  };
  //监听页面初始的searchParam
  useEffect(() => {
    setValue(searchParam.get('search') || '');
  }, [searchParam]);
  return (
    <Search
      onSearch={onSearch}
      onChange={e => {
        setValue(e.target.value);
      }}
      value={value}
      style={{ width: '200px' }}
    />
  );
};

export default ListSearch;
