import { FunctionComponent, useState } from 'react';
import styles from './questionCard.module.scss';
import { Link, useNavigate } from 'react-router-dom';
import { Button, Space, Tag, Popconfirm, Modal, message } from 'antd';
import { useRequest } from 'ahooks';
import {
  EditOutlined,
  LineChartOutlined,
  StarOutlined,
  CopyOutlined,
  DeleteOutlined,
  StarFilled,
} from '@ant-design/icons';
import { updateQuestion, duplicateQuestion } from '../../api/question';
interface QuestionCardProps {
  id: string;
  title: string;
  isStar: boolean;
  isPublished: boolean;
  answerCount: number;
  createAt: string;
  onDelete: (id: string | number) => void;
}

const QuestionCard: FunctionComponent<QuestionCardProps> = props => {
  const { id, title, isStar, isPublished, createAt, answerCount, onDelete } =
    props;
  const { confirm } = Modal;
  const nav = useNavigate();
  const [isStarState, setIsStarState] = useState(isStar);
  //标星请求
  const { loading: changeStarLoading, run: changeStar } = useRequest(
    () => {
      return updateQuestion(id, { isStar: !isStarState });
    },
    {
      manual: true,
      onSuccess() {
        setIsStarState(!isStarState);
        message.success('更新完成');
      },
    },
  );
  //复制请求
  const { loading: duplicateLoading, run: duplicate } = useRequest(
    () => {
      return duplicateQuestion(id);
    },
    {
      manual: true,
      onSuccess(res) {
        message.success(`复制成功---${res.id}`);
        nav(`/question/edit/${res.id}`);
      },
    },
  );
  // 删除相关方法
  const [isDeletedState, setIsDeletedState] = useState(false);
  const { loading: deleteLoading, run: deleteQuestion } = useRequest(
    () => {
      return updateQuestion(id, { isDeleted: true });
    },
    {
      manual: true,
      onSuccess(res) {
        message.success(`删除成功---${res.id}`);
        setIsDeletedState(true);
      },
    },
  );
  const handlerDelete = () => {
    confirm({
      title: '确认要删除吗？',
      onOk() {
        deleteQuestion();
        onDelete && onDelete(id);
      },
    });
  };
  //设置标记，如果已经被删除则不会被渲染
  if (isDeletedState) return null;
  return (
    <>
      <div className={styles.container}>
        <div className={styles.top}>
          <div className={styles.topLeft}>
            <Link
              to={isPublished ? `/question/stat/${id}` : `/question/edit/${id}`}
            >
              <Space size={'small'}>
                {isStarState && <StarOutlined style={{ color: 'red' }} />}
                {title}
              </Space>
            </Link>
          </div>
          <div className={styles.topRight}>
            <Space>
              {isPublished ? (
                <Tag color="processing">已发布</Tag>
              ) : (
                <Tag>未发布</Tag>
              )}
              <div>答卷 {answerCount}</div>
              <div>{createAt}</div>
            </Space>
          </div>
        </div>
        <div className={styles.bottom}>
          <div className={styles.bottomLeft}>
            <Space>
              <Button
                type="text"
                icon={<EditOutlined />}
                size="small"
                onClick={() => nav(`/question/edit/${id}`)}
              >
                编辑问卷
              </Button>
              <Button
                disabled={!isPublished}
                type="text"
                icon={<LineChartOutlined />}
                size="small"
                onClick={() => nav(`/question/stat/${id}`)}
              >
                统计数据
              </Button>
            </Space>
          </div>
          <div className={styles.bottomRight}>
            <Space>
              <Button
                type="text"
                onClick={changeStar}
                loading={changeStarLoading}
                icon={
                  !isStarState ? (
                    <StarOutlined />
                  ) : (
                    <StarFilled style={{ color: 'red' }} />
                  )
                }
                size="small"
              >
                {isStarState ? '取消标星' : '标星'}
              </Button>
              <Popconfirm title="确定要复制吗？" onConfirm={duplicate}>
                <Button
                  type="text"
                  icon={<CopyOutlined />}
                  size="small"
                  loading={duplicateLoading}
                >
                  复制
                </Button>
              </Popconfirm>
              <Button
                type="text"
                icon={<DeleteOutlined />}
                onClick={handlerDelete}
                loading={deleteLoading}
                size="small"
              >
                删除
              </Button>
            </Space>
          </div>
        </div>
      </div>
    </>
  );
};

export default QuestionCard;
