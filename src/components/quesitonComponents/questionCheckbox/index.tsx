import { FunctionComponent } from 'react';
import { QuestionCheckboxPropsType, QuestionCheckboxDefaultProps } from './interface';
import { Checkbox, Space, Typography } from 'antd';
import PropComponent from './propComponent';
const QuestionCheckbox: FunctionComponent<QuestionCheckboxPropsType> = (props: QuestionCheckboxPropsType) => {
  const { isVertical, list, title } = props;
  return (
    <>
      <Typography.Paragraph>{title}</Typography.Paragraph>
      <Space direction={isVertical ? 'vertical' : 'horizontal'}>
        {list?.map((o, index) => {
          const { text, checked } = o;
          return (
            <Checkbox key={index} checked={checked}>
              {text}
            </Checkbox>
          );
        })}
      </Space>
    </>
  );
};

export const questionCheckboxConf = {
  type: 'questionCheckbox',
  title: '多选框',
  Component: QuestionCheckbox,
  PropComponent,
  defaultProps: QuestionCheckboxDefaultProps,
};
export default QuestionCheckbox;
