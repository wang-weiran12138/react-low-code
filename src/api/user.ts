import axiosInstance from '../utils/request';
interface ResDataType<T> {
  code: number;
  data?: T;
  message: string;
}
interface Token {
  token: string;
}
interface loginData {
  username: string;
  password: string;
}
interface registerData extends loginData {
  password_confirm: string;
}
//注册
export const registerService: (
  data: registerData,
) => Promise<ResDataType<Token>> = data => {
  return axiosInstance({ url: '/auth/register', method: 'Post', data });
};
//登录
export const loginService = (data: loginData): Promise<ResDataType<Token>> => {
  return axiosInstance({ url: '/auth/login', method: 'Post', data });
};
//获取用户登录信息
export const userInfoService = (): Promise<ResDataType<any>> => {
  return axiosInstance({ url: '/auth/userInfo' });
};

export const getUserCategory = () => {
  return '123';
};
